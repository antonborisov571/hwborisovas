﻿namespace ClassWork2
{
    public class SchoolStudent : Student, IAboutMeTellable
    {
        private byte _grade;
        public byte Grade 
        { 
            get
            {
                return _grade;
            }
            set
            {
                if (value <= 11 && value >= _grade+1 && value > 0)
                {
                    _grade = value;
                }
            }
        }
        public string Profile { get; set; }
        public SchoolStudent(string name, StudyType studyType, byte grade) : base(name)
        {
            Grade = grade;
            StudyType = StudyType.School;
        }

        public string SayAboutMe()
        {
            return "Я студент";
        }

        void IAboutMeTellable.SayAboutMe()
        {
            throw new NotImplementedException();
        }
    }
}
