﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork2
{
    public class Student
    {
        public string FullName { get; init; }
        public StudyType StudyType { get; set; }
        public ushort Age { get; set; }
        public DateOnly StartStudy { get; set; }
        public DateOnly EndStudy { get; set; }
        public Student(string fullName, StudyType studyType = StudyType.University)
        {
            FullName = fullName;
            StudyType = studyType;
        }
        public Student(DateOnly startDate, DateOnly endDate)
        {
            StartStudy = startDate;
            EndStudy = endDate;
        }
        public virtual string Study()
        {
            return "Студент учиться";
        }
    }

}
