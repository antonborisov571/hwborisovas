﻿namespace ClassWork2
{
    public class UniversityStudent : Student
    {
        public byte Course { get; set; }
        public UniversityStudent(string name, byte course) : base(name)
        {
            Course = course;
            StudyType = StudyType.University;
        }
    }
}
