﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork2
{
    public interface IAboutMeTellable
    {
        void SayAboutMe();
    }
}
