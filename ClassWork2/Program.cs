﻿using ClassWork2;

var student = new Student("Borisov Anton Sergeevich");
var student2 = new Student("Borisov Anton Sergeevich", StudyType.University);
var student3 = new Student(new DateOnly(2022, 09, 01), new DateOnly(2026, 09, 01));
var studentWithInitialize = new Student("Borisov Anton Sergeevich")
{
    Age = 18,
    FullName = "Borisov Anton Sergeevich",
    StudyType = StudyType.University
};
Console.WriteLine(studentWithInitialize.FullName); 