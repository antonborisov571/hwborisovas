﻿using System;

namespace Test1
{
    class Program
    {
        static void Main(string[] args)
        {
		}
		#region 2ex
		public static double SharkArray(double[] inputData)
        {
			if (inputData.Length <= 2)
			{
				throw new ArgumentException("Неверно введены данные");
			}
			for (int i = 1; i < inputData.Length-1; i++)
			{
				if (!((inputData[i-1] > inputData[i] && inputData[i+1] > inputData[i]) 
					||(inputData[i-1] < inputData[i] && inputData[i+1] < inputData[i])))
					{
						return inputData[i];
					}
			}
			return 0;
        }
		#endregion 2ex
		#region 6ex
		public static long CheckEvenOddSequence(long[] inputArray)
        {
			if (inputArray.Length <= 2)
			{
				throw new ArgumentException("Неверно введены данные");
			}
			for (int i = 0; i < inputArray.Length - 1; i++)
			{
				if (!(inputArray[i]%2 != inputArray[i+1]%2))
				{
					return inputArray[i+1];
				}
			}
			return 0;
        }
		#endregion 6ex
		#region 11ex
		public static string[] LeftRightMatrixRead(long[,] inputArray)
        {
			string[] matrix = new string[inputArray.GetLength(0)];
			string dopString; // Дополнительная строка в которой я буду собирать строку
			if (inputArray.GetLength(0) < 2 || inputArray.GetLength(1) < 3)
			{
				throw new ArgumentException("Неверно введены данные");
			}
			for (int i = 0; i < inputArray.GetLength(0); i++)
			{
				dopString = "";
				if (i%2==0)
				{
					for (int j = 0; j < inputArray.GetLength(1); j++)
					{
						dopString += inputArray[i, j].ToString()+" ";
					}
					matrix[i] = dopString;
				}
				else
				{
					for (int j = inputArray.GetLength(1)-1; j >= 0; j--)
					{
						dopString += inputArray[i, j].ToString()+" ";
					}
					matrix[i] = dopString;
				}
			}
			return matrix;
        }
		#endregion 11ex
    }
}
