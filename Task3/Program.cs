﻿using System;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(GetOperationResult(10, 0, '/'));
        }
		#region ClassWork
		static decimal GetOperationResult(decimal opOne, decimal opTwo, char operation)
		{
			#region if(else)
			/*
			if (operation == '/')
			{
				if (opTwo == 0)
				{
					Console.WriteLine("Деление на 0 запрещено");
					return default;
				}
				return opOne/opTwo;
			}
			if (operation == '*')
			{
				return opOne * opTwo;
			}
			if (operation == '+')
			{
				return opOne + opTwo;
			}
			if (operation == '-')
			{
				return opOne - opTwo;
			}
			return 0;
			*/
			//TODO: Метод выведения результата операции
			#endregion if(else)
			#region switch
			/*
			switch (operation)
			{
				case '/':
					switch (opTwo)
					{
						case 0:
							Console.WriteLine("Деление на 0 запрещено");
							return default;
						default: return opOne / opTwo;
					}
				case '*': return opOne * opTwo;
				case '+': return opOne + opTwo;
				case '-': return opOne - opTwo;
				default: return default;
			}
			*/
			#endregion switch
			#region TernaryOperator
			return (operation == '/' && opTwo == 0 ? default
			: (opTwo != 0 && operation == '/' ? opOne / opTwo
			: (operation == '*' ? opOne * opTwo
			: (operation == '+' ? opOne + opTwo
			: (operation == '-' ? opOne - opTwo : 0)))));
			#endregion TernaryOperator
		}
		#endregion ClassWork
    }
}
