﻿using System;

namespace DistanceTask
{
	public static class DistanceTask
	{
        // Расстояние от точки (x, y) до отрезка AB с координатами A(ax, ay), B(bx, by)
        static double infinity = 10000000000;
        public static double GetDistanceToSegment(double ax, double ay, double bx, double by, double x, double y) 
        {
            double tangentLine = (bx - ax) != 0 ? (by - ay) / (bx - ax) : infinity;
            double coefficient = tangentLine != infinity ? ay - tangentLine * ax : 0;
            double coefficientOther = tangentLine != infinity ? y + (1 / tangentLine) * x : 0;
            double intersectionX = GetIntersectionX(ax, x, tangentLine, coefficient, coefficientOther);
            double intersectionY = GetIntersectionY(y, tangentLine, coefficient, intersectionX);
            if (GetIntersectionInSegment(ax, ay, bx, by, intersectionX, intersectionY)) 
            {
                return tangentLine != infinity ? GetDistance(x, y, tangentLine, coefficient)
                    : Math.Abs(x - ax);
            }
            else return Math.Min(GetDistancePoints(ax, ay, x, y), GetDistancePoints(bx, by, x, y));
        }

        public static double GetDistance(double x, double y, double tangentLine, double coefficient) 
        {
            return Math.Abs(-tangentLine * x + y - coefficient) / Math.Sqrt(tangentLine * tangentLine + 1);
        }

        public static double GetDistancePoints(double ax, double ay, double x, double y) 
        {
            return Math.Sqrt((x - ax) * (x - ax) + (y - ay) * (y - ay));
        }

        public static bool GetIntersectionInSegment(double ax, double ay, double bx, double by, double intersectionX, double intersectionY) 
        {
            return (ax <= intersectionX && intersectionX <= bx && ay <= intersectionY && intersectionY <= by) ||
                   (bx <= intersectionX && intersectionX <= ax && by <= intersectionY && intersectionY <= ay) ||
                   (ax <= intersectionX && intersectionX <= bx && by <= intersectionY && intersectionY <= ay) ||
                   (bx <= intersectionX && intersectionX <= ax && ay <= intersectionY && intersectionY <= by);
        }

        public static double GetIntersectionY(double y, double tangentLine, double coefficient, double intersectionX) 
        {
            return tangentLine != infinity ? tangentLine * intersectionX + coefficient : y;
        }

        public static double GetIntersectionX(double ax, double x, double tangentLine, double coefficient, double coefficientOther) 
        {
            return tangentLine != 0 && tangentLine != infinity
                ? (-coefficient + coefficientOther) / (tangentLine + 1 / tangentLine)
                : (tangentLine == 0 ? x : ax);
        }
    }
}