﻿namespace Pluralize
{
	public static class PluralizeTask
	{
		public static string PluralizeRubles(int count)
		{
			// Напишите функцию склонения слова "рублей" в зависимости от предшествующего числительного count.
			if (count % 10 == 1 && count % 100 != 11) return "рубль";
			else if (2 <= count % 10 && count % 10 <= 4 && !(12 <= count % 100 && count % 100 <= 14)) return "рубля";
			else return "рублей";
		}
	}
}