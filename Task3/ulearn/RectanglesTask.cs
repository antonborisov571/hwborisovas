﻿using System;

namespace Rectangles
{
	public static class RectanglesTask
	{
		// Пересекаются ли два прямоугольника (пересечение только по границе также считается пересечением)
		public static bool AreIntersected(Rectangle r1, Rectangle r2)
		{
			// так можно обратиться к координатам левого верхнего угла первого прямоугольника: r1.Left, r1.Top
			if (r1.Top + r1.Height < r2.Top) return false;
			else if (r1.Left + r1.Width < r2.Left) return false;
			else if (r2.Top + r2.Height < r1.Top) return false;
			else return !(r2.Left + r2.Width < r1.Left);
		}

		// Площадь пересечения прямоугольников
		public static int IntersectionSquare(Rectangle r1, Rectangle r2) 
		{
			if (AreIntersected(r1, r2)) 
			{
                if (GetHigher(r1, r2) && GetLongestHeight(r1, r2)) 
                    return GetIntersectionWidth(r1, r2) * GetIntersectionHeight(r1, r2);
                else if (GetHigher(r2, r1) && GetLongestHeight(r2, r1)) 
                    return GetIntersectionWidth(r2, r1) * GetIntersectionHeight(r2, r1);
                else if (GetHigher(r1, r2)) return GetIntersectionHeight(r1, r2) * GetIntersectionWidth(r1, r2);
                else if (GetHigher(r2, r1)) return GetIntersectionHeight(r2, r1) * GetIntersectionWidth(r2, r1);
            }
            return 0;
        }

        public static bool GetHigher(Rectangle r1, Rectangle r2) 
        {
            return r1.Top <= r2.Top;
        }

        public static bool GetLongestHeight(Rectangle r1, Rectangle r2)
        {
            return r1.Top + r1.Height >= r2.Top + r2.Height;
        }

        public static int GetIntersectionWidth(Rectangle r1, Rectangle r2) 
		{
            if (r1.Left <= r2.Left && r1.Left + r1.Width >= r2.Left + r2.Width) return r2.Width;
            else if (r1.Left <= r2.Left) return r1.Left + r1.Width - r2.Left;
            else if (r1.Left + r1.Width >= r2.Left + r2.Width) return r2.Left + r2.Width - r1.Left;
            else return r1.Width;
            return 0;
        }

        public static int GetIntersectionHeight(Rectangle r1, Rectangle r2) 
        { 
            if (r1.Top <= r2.Top && r1.Top + r1.Height >= r2.Top + r2.Height) return r2.Height;
            else if (r1.Top <= r2.Top) return r1.Top + r1.Height - r2.Top;
            else if (r1.Top + r1.Height >= r2.Top + r2.Height) return r2.Top + r2.Height - r1.Top;
            else return r1.Height;
            return 0;
        }

        // Если один из прямоугольников целиком находится внутри другого — вернуть номер (с нуля) внутреннего.
        // Иначе вернуть -1
        // Если прямоугольники совпадают, можно вернуть номер любого из них.
        public static int IndexOfInnerRectangle(Rectangle r1, Rectangle r2)
		{
            if (GetHigher(r1, r2) && GetLongestHeight(r1, r2) && GetLongestWidth(r1, r2)) return 1;
            else if (GetHigher(r2, r1) && GetLongestHeight(r2, r1) && GetLongestWidth(r2, r1)) return 0;
            else return -1;
		}

        public static bool GetLongestWidth(Rectangle r1, Rectangle r2) 
        {
            return r1.Left <= r2.Left && r1.Left + r1.Width >= r2.Left + r2.Width;
        }
    }
}