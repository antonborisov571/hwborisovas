﻿using System;
using System.Linq;

namespace Names
{
    internal static class HistogramTask
    {
        public static HistogramData GetBirthsPerDayHistogram(NameData[] names, string name)
        {
            double[] birthDay = new double[31];
            string[] days = new string[31];
            for (int i = 1; i < 32; i++)
            {
                days[i-1] = i.ToString(); 
            }
            foreach (var human in names)
            {
                if (human.Name == name && human.BirthDate.Day != 1)
                    birthDay[human.BirthDate.Day-1]++;
            }
            return new HistogramData(
                string.Format("Рождаемость людей с именем '{0}'", name), 
                days, 
                birthDay);
        }
    }
}