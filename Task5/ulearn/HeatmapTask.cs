﻿using System;

namespace Names
{
    internal static class HeatmapTask
    {
        public static HeatmapData GetBirthsPerDateHeatmap(NameData[] names)
        {
            string[] days = new string[30];
            for (int i = 2; i < 32; i++)
                days[i - 2] = i.ToString();
            string[] mounth = new string[12];
            for (int i = 1; i < 13; i++)
                mounth[i - 1] = i.ToString();
            double[,] birthDayMonth = new double[30, 12];
            foreach (var human in names)
            {
                if (human.BirthDate.Day != 1)
                    birthDayMonth[human.BirthDate.Day - 2, human.BirthDate.Month - 1]++;
            }
            return new HeatmapData(
                "Пример карты интенсивностей",
                birthDayMonth, 
                days, 
                mounth);
        }
    }
}