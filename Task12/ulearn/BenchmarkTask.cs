using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using NUnit.Framework;
using StructBenchmarking;

namespace StructBenchmarking
{
    public class Benchmark : IBenchmark
	{
        public double MeasureDurationInMs(ITask task, int repetitionCount)
        {
            GC.Collect();                   // Эти две строчки нужны, чтобы уменьшить вероятность того,
            GC.WaitForPendingFinalizers();  // что Garbadge Collector вызовется в середине измерений
                                            // и как-то повлияет на них.
            task.Run();
            var time = Stopwatch.StartNew();
            time.Restart();
            for (var i = 0; i < repetitionCount; i++)
            {
                task.Run();   
            }
            return time.Elapsed.TotalMilliseconds/repetitionCount;
			//throw new NotImplementedException();
		}
	}

    public class CreateStringBuilder : ITask
    {
        public void Run()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (var i = 0; i < 10000; i++)
            {
                stringBuilder.Append('a');
            }
            stringBuilder.ToString();
        }
    }

    public class CreateStringConstructor : ITask
    {
        public void Run()
        {
            var str = new string('a', 10000);
        }
    }

    [TestFixture]
    public class RealBenchmarkUsageSample
    {
        [Test]
        public void StringConstructorFasterThanStringBuilder()
        {
            var createStringBuilder = new CreateStringBuilder();
            var createStringConstructor = new CreateStringConstructor();
            var benchmark = new Benchmark();
            var time1 = benchmark.MeasureDurationInMs(createStringBuilder, 100000);
            var time2 = benchmark.MeasureDurationInMs(createStringConstructor, 100000);
            Assert.Less(time2, time1);
        }
    }
}