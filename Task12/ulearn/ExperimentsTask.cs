using System.Collections.Generic;

namespace StructBenchmarking
{
    public class Experiments
    {
        public static ChartData BuildChartDataForArrayCreation(
            IBenchmark benchmark, int repetitionsCount)
        {
            var classesTimes = new List<ExperimentResult>();
            var structuresTimes = new List<ExperimentResult>();
            ClassArrayCreationTask classes;
            StructArrayCreationTask structures;
            double time1;
            double time2;
            foreach (var constant in Constants.FieldCounts)
            {
                classes = new ClassArrayCreationTask(constant);
                structures = new StructArrayCreationTask(constant);
                time1 = benchmark.MeasureDurationInMs(classes, repetitionsCount);
                time2 = benchmark.MeasureDurationInMs(structures, repetitionsCount);
                classesTimes.Add(new ExperimentResult(constant, time1));
                structuresTimes.Add(new ExperimentResult(constant, time2));
            }
            return new ChartData
            {
                Title = "Create array",
                ClassPoints = classesTimes,
                StructPoints = structuresTimes,
            };
        }

        public static ChartData BuildChartDataForMethodCall(
            IBenchmark benchmark, int repetitionsCount)
        {
            var classesTimes = new List<ExperimentResult>();
            var structuresTimes = new List<ExperimentResult>();
            MethodCallWithClassArgumentTask classes;
            MethodCallWithStructArgumentTask structures;
            double time1;
            double time2;
            foreach (var constant in Constants.FieldCounts)
            {
                classes = new MethodCallWithClassArgumentTask(constant);
                structures = new MethodCallWithStructArgumentTask(constant);
                time1 = benchmark.MeasureDurationInMs(classes, repetitionsCount);
                time2 = benchmark.MeasureDurationInMs(structures, repetitionsCount);
                classesTimes.Add(new ExperimentResult(constant, time1));
                structuresTimes.Add(new ExperimentResult(constant, time2));
            }
            //...

            return new ChartData
            {
                Title = "Call method with argument",
                ClassPoints = classesTimes,
                StructPoints = structuresTimes,
            };
        }
    }
}