using System;

namespace Calculate
{
    class CalculateTask
    {		
        static void Main(string[] args)
        {	
			Console.WriteLine(Calculate(Console.ReadLine()));
		}
		public static double Calculate(string userInput)
		{
			var parts = userInput.Split();
			double money = double.Parse(parts[0]);
			double percent = double.Parse(parts[1]);
			double months = double.Parse(parts[2]);
			return Math.Round((money * (double)Math.Pow((1 + percent/(100*12)), months)), 15);
		}
	}
}