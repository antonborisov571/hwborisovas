using System;

namespace Task2
{
    class Program
    {		
        static void Main(string[] args)
        {	
			#region (questionnaire)

			string fio = Console.ReadLine();
			byte age = byte.Parse(Console.ReadLine());
			byte course = byte.Parse(Console.ReadLine());
			string groupName = Console.ReadLine();
			bool hasAnimal = Console.ReadLine() == "Да";
			Console.WriteLine("Меня зовут {0}. Мне {1} лет.\nУчусь в КФУ ИТИС на курсе {2} в группе номер {3}.\nЕсть ли домашнее животное? Я вам скажу: {4}", fio, age, course, groupName, hasAnimal);
			#endregion (questionnaire)
		}
	}
}
