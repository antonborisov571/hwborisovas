﻿using System.Text;

public class Program
{
    //variant 4
    static void Main(string[] args)
    {
    }
    #region 1ex
    public static string InsertSogBukv(string? str)
    {
        if (str == null) throw new ArgumentNullException(nameof(str));
        var strSogBukvs = new char[] { 'q', 'w', 'r', 't', 'p', 's', 'd', 'f', 
            'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm',
            'Q', 'W', 'R', 'T', 'P', 'S', 'D', 'F','G', 'H', 'J', 'K', 
            'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'};
        var resultStr = new StringBuilder(str);
        for (var i = 0; i < str.Length; i++)
        {
            foreach (var sogBukva in strSogBukvs)
            {
                if (sogBukva == str[i])
                {
                    resultStr[i] = '&';
                }
            }
        }
        return resultStr.ToString();
    }
    #endregion 1ex
    #region 2ex
    public static string GetNameFile(string? directory)
    {
        if (directory == String.Empty || directory == null) throw new ArgumentNullException(nameof(directory));
        var indexDot = directory.LastIndexOf('.');
        var indexSlash = directory.LastIndexOf('\\');
        return directory.Substring(indexSlash+1, indexDot - indexSlash-1);
    }
    #endregion 2ex
    #region 3ex
    public static int[] RaznostArrays(int[]? arr1, int[]? arr2)
    {
        if (arr1.Length == 0 || arr2.Length == 0) return arr1;
        if (arr1 == null) throw new ArgumentNullException(nameof(arr1));
        if (arr2 == null) throw new ArgumentNullException(nameof(arr2));
        for (var i = 0; i < arr1.Length; i++)
        {
            for (var j = 0; j < arr2.Length; j++)
            {
                if (arr1[i] == arr2[j])
                {
                    arr1[i] = int.MaxValue;//костыль
                    break;
                }
            }
        }
        Array.Sort(arr1);
        var arr = arr1[0..(Array.IndexOf(arr1, int.MaxValue))];
        return arr1[0..Array.IndexOf(arr1, int.MaxValue)];
    }
    #endregion 3ex
}