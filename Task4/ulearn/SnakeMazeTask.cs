﻿namespace Mazes
{
	public static class SnakeMazeTask
	{
		public static void MoveOut(Robot robot, int width, int height)
		{
			height -= 2;
			while (height - 2  > 0)
			{
				MoveRightMinus3(robot, width);
				MoveDownStep2(robot);
				MoveLeftMinus3(robot, width);
				if (height - 4 > 0)
					MoveDownStep2(robot);
				height -= 4;
			}
		}

		public static void MoveRightMinus3(Robot robot, int width)
		{
			for (int i = 0; i < width - 3; i++)
				robot.MoveTo(Direction.Right);
		}

		public static void MoveLeftMinus3(Robot robot, int width)
		{
			for (int i = 0; i < width - 3; i++)
				robot.MoveTo(Direction.Left);
		}

		public static void MoveDownStep2(Robot robot)
		{
			for (int i = 0; i < 2; i++)
				robot.MoveTo(Direction.Down);
		}
	}
}