﻿using System;

namespace Mazes
{
	public static class DiagonalMazeTask
	{
		public static void MoveOut(Robot robot, int width, int height)
		{
			float diagonal = 0;
			MoveIfWidthMoreHeight(robot, width, height, diagonal);
			MoveIfHeightMoreWidth(robot, width, height, diagonal);
		}

		public static void MoveIfWidthMoreHeight(Robot robot, int width, int height, float diagonal)
        {
			if (width > height)
			{
				diagonal = (width / height) + 1;
				height -= 2;
				while (height > 0)
				{
					MoveRight(robot, diagonal);
					if (height - 1 > 0) robot.MoveTo(Direction.Down);
					height -= 1;
				}
			}
		}

		public static void MoveIfHeightMoreWidth(Robot robot, int width, int height, float diagonal)
        {
			if (width < height)
			{
				diagonal = (float)height / (float)width;
				width -= 2;
				while (width > 0)
				{
					MoveDown(robot, diagonal);
					if (width - 1 > 0) robot.MoveTo(Direction.Right);
					width -= 1;
				}
			}
		}

		public static void MoveDown(Robot robot, float diagonal)
		{
			for (int i = 0; i < Math.Round(diagonal); i++)
				robot.MoveTo(Direction.Down);
		}

		public static void MoveRight(Robot robot, float diagonal)
		{
			for (int i = 0; i < diagonal; i++)
				robot.MoveTo(Direction.Right);
		}
	}
}