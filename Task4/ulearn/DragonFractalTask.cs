﻿using System.Drawing;
using System;

namespace Fractals
{
	internal static class DragonFractalTask
	{
		public static void DrawDragonFractal(Pixels pixels, int iterationsCount, int seed)
		{
			var random = new Random(seed);
			var nextNumber = random.Next(2);
			double x = 1;
			double y = 0;
			pixels.SetPixel(x, y);
			for (int i = 0; i < iterationsCount; i++)
			{
				nextNumber = random.Next(2);
				if (nextNumber == 1)
                {
                    Transform1(ref x, ref y);
                    pixels.SetPixel(x, y);
                }
                else
                {
                    Transform2(ref x, ref y);
                    pixels.SetPixel(x, y);
                }
            }
		}

        public static void Transform2(ref double x, ref double y)
        {
            var x1 = ((x * Math.Cos(3 * Math.PI / 4) - y * Math.Sin(3 * Math.PI / 4)) / Math.Sqrt(2) + 1);
            var y1 = ((x * Math.Sin(3 * Math.PI / 4) + y * Math.Cos(3 * Math.PI / 4)) / Math.Sqrt(2));
            x = x1;
            y = y1;
        }

        public static void Transform1(ref double x, ref double y)
        {
            var x1 = ((x * Math.Cos(Math.PI / 4) - y * Math.Sin(Math.PI / 4)) / Math.Sqrt(2));
            var y1 = ((x * Math.Sin(Math.PI / 4) + y * Math.Cos(Math.PI / 4)) / Math.Sqrt(2));
            x = x1;
            y = y1;
        }
    }
}