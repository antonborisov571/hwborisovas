﻿namespace Mazes
{
	public static class EmptyMazeTask
	{
		public static void MoveOut(Robot robot, int width, int height)
        {
            MoveDownMinus3(robot, height);
            MoveRightMinus3(robot, width);
        }

        public static void MoveRightMinus3(Robot robot, int width)
        {
            for (int i = 0; i < width - 3; i++) 
                robot.MoveTo(Direction.Right);
        }

        public static void MoveDownMinus3(Robot robot, int height)
        {
            for (int i = 0; i < height - 3; i++) 
                robot.MoveTo(Direction.Down);
        }
    }
}