﻿using System;

namespace Task4
{
    public class Program
    {
        static void Main(string[] args)
        {
			Console.WriteLine(NumberContainsThreeSameNumbers(111));
			char a = 'a';
			a++;
			Console.WriteLine(a);
        }
		#region ClassWork
		#region Fibonacci
		public static void Fibonacci(int n) 
		{
			#region while
			/*
			int a = 0;
			int b = 1;
			int c;
			while(b < n)
			{
				c = b;
				b = b + a;
				a = c;
				Console.WriteLine(a);
			*/
			#endregion while
			#region for
			/*
			int c;
			int a = 0;
			int b = 1;
			for (;;)
			{
				if (b < n)
				{
					c = b;
					b = b + a;
					a = c;
					Console.WriteLine(a);
				}
				else break;
			}
			*/
			#endregion for
		}
		#region recursion
		public static int ValueFib(int n)
		{
			if (n == 0) return 0;
			else if (n == 1) return 1;
			else if (n > 1) return ValueFib(n-2) + ValueFib(n-1);
			else throw new ArgumentException("Передан некорректный аргумент");
		}
		public static void Fib(int n)
		{
			for (int i = 0; i <= n; i++)
			{
				if (ValueFib(i) <= n) Console.WriteLine(ValueFib(i));
				else break;
			}
		}
		#endregion recursion
		#endregion Fibonacci
		#region CheckPalindrome
		public static bool CheckPalindrome(int number)
		{
			string strNumber = number.ToString();
			if (1 > number || 9999 < number)
			{
				throw new ArgumentException($"Передан некорректный аргумент n {typeof(int)}");
			}
			else return CheckFour(strNumber) || CheckThree(strNumber) 
				|| CheckTwo(strNumber) || strNumber.Length == 1;
		}
		public static bool CheckFour(string strNumber)
		{
			return strNumber.Length == 4 && strNumber[0..2] == $"{strNumber[3]}{strNumber[2]}";
		}
		public static bool CheckThree(string strNumber)
		{
			return strNumber.Length == 3 && strNumber[0] == strNumber[2];
		}
		public static bool CheckTwo(string strNumber)
		{
			return strNumber.Length == 2 && strNumber[0] == strNumber[1];
		}
		#endregion CheckPalindrome
		#region NumberContainsThreeSameNumbers
		public static bool NumberContainsThreeSameNumbers(int number)
		{
			if (number < 100 || number > 9999) 
				throw new ArgumentException("Передан некорректный аргумент");
			return GetRepeatingNumber(number); 
		}
		public static bool GetRepeatingNumber(int number)
		{
			int countDigit;
			int maxCountDigit = 0;
			string strNumber = number.ToString();
			for (int i = 0; i < 10; i++)
			{
				countDigit = 0;
				for (int j = 0; j < strNumber.Length; j++)
					if (i.ToString().ToCharArray()[0] == strNumber[j]) countDigit++;
				maxCountDigit = Math.Max(maxCountDigit, countDigit);
			}
			return maxCountDigit > 2;
		}
		#endregion NumberContainsThreeSameNumbers
		#endregion ClassWork
	}
}
