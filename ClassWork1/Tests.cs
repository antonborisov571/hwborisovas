using System;
using Task4;

namespace ClassWork1
{
	public class UnitTest1
	{
		public static bool PalindromeTest(int number)
        {
			return Program.CheckPalindrome(number);
		}
		public static bool ThreeNumbersTest(int number)
        {
			return Program.NumberContainsThreeSameNumbers(number);
		}
		[Fact]
		public void PalindromeTest1()
        {
			Assert.Equal(true, PalindromeTest(22));
		}
		[Fact]
		public void PalindromeTest2()
		{
			Assert.Equal(false, PalindromeTest(224));
		}
		[Fact]
		public void ThreeNumberTest1()
		{
			Assert.Equal(true, ThreeNumbersTest(222));
		}
		[Fact]
		public void ThreeNumberTest2()
		{
			Assert.Equal(false, ThreeNumbersTest(2233));
		}
	}
	public class UnitTest2
	{
		#region (SA)
		public static double SharkArrayTest(params double[] arr)
        {
			return Exersize.SharkArray(arr);
        }
		[Fact]
		public void SATest1()
        {
			Assert.Equal(2, SharkArrayTest(1, 2, 3));
        }
		[Fact]
		public void SATest2()
		{
			Assert.Equal(0, SharkArrayTest(1, 3, 1));
		}
		#endregion (SA)
		#region CEOS
		public static long CheckEvenOddSequenceTest(params long[] arr)
		{
			return Exersize.CheckEvenOddSequence(arr);
		}
		[Fact]
		public void CEOSTest1()
        {
			Assert.Equal(0, CheckEvenOddSequenceTest(1, 2, 3, 4, 5, 6, 7));
        }
		[Fact]
		public void CEOSTest2()
		{
			Assert.Equal(8, CheckEvenOddSequenceTest(1, 2, 3, 4, 5, 6, 8));

		}
		#endregion CEOS
	}
}