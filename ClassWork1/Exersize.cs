﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork1
{
	internal class Exersize
	{
		public static double SharkArray(double[] inputData)
		{
			if (inputData.Length <= 2)
			{
				throw new ArgumentException("Неверно введены данные");
			}
			for (int i = 1; i < inputData.Length - 1; i++)
			{
				if (!((inputData[i - 1] > inputData[i] && inputData[i + 1] > inputData[i])
					|| (inputData[i - 1] < inputData[i] && inputData[i + 1] < inputData[i])))
				{
					return inputData[i];
				}
			}
			return 0;
		}
		public static long CheckEvenOddSequence(long[] inputArray)
		{
			if (inputArray.Length <= 2)
			{
				throw new ArgumentException("Неверно введены данные");
			}
			for (int i = 0; i < inputArray.Length - 1; i++)
			{
				if (!(inputArray[i] % 2 != inputArray[i + 1] % 2))
				{
					return inputArray[i + 1];
				}
			}
			return 0;
		}
	}
}
