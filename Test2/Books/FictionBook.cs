﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test2.Books
{
    public class FictionBook
    {
        public BookType BookType { get; set; }
        public Genre Genre { get; init; }
    }
}
