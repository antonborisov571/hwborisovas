﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test2.Library;

namespace Test2.Books
{
    public class RentBook
    {
        public Book Book { get; set; }
        public DateOnly DateForRent { get; set; }
        public LibraryClient Client { get; set; }
    }
}
