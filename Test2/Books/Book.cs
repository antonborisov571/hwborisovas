﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test2.Books
{
    public abstract class Book
    {
        public abstract string Name { get; init; }
        public abstract string Author { get; init; }
        public abstract uint PageCount { get; set; }
        public abstract DateOnly DateOfCreated { get; init; }
        public string KeyWords { get; set; }
        public abstract BookType BookType { get; init; }

    }
}
