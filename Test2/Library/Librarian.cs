﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test2.Books;

namespace Test2.Library
{
    public class Librarian
    {
        private readonly string _keyWordsForAdults;
        private readonly List<RentBook> _rentedBooks = new();
        public LibraryClient RegisteredNewClient(string fullName, uint age)
        {
            LibraryClientCounter.ClientCount++;
            return new LibraryClient
            {
                Age = age,
                FullName = fullName,
                LibraryClientNumber = LibraryClientCounter.ClientCount - 1
            };
        }
        public bool TryRentBooks(Book book, LibraryClient client)
        {
            if (client.RentBook.Count > 3) return false;
            if (client.Age > 18) return false;
            return true;
        }
        public bool CheckRentBook(Book book)
        {
            return false;
        }
        Librarian()
        {

        }
    }
}
