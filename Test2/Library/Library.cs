﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test2.Books;

namespace Test2.Library
{
    public class Library
    {
        private readonly List<Book> _books;
        private readonly Librarian _librarian;
        private readonly List<LibraryClient> _clients = new();
        public Book FindBookByName(string name)
        {
            foreach (Book book in _books)
                if (book.Name == name)
                    return book;
            return null;
        }
        public LibraryClient Registration(string fullName, uint age)
        {
            LibraryClientCounter.ClientCount++;
            return new LibraryClient
            {
                Age = age,
                FullName = fullName,
                LibraryClientNumber = LibraryClientCounter.ClientCount - 1
            };
        }
        public List<Book> FindBooksByAuthor(string author)
        {
            List<Book> books = new List<Book>();
            foreach (Book book in _books)
            {
                if (book.Author == author)
                    books.Add(book);
            }
            return books;
        }
        public List<Book> FindBooksByType(BookType bookType)
        {
            List<Book> books = new List<Book>();
            foreach (Book book in _books)
            {
                if (book.BookType == bookType)
                    books.Add(book);
            }
            return books;
        }
        public bool RentBook(Book book, LibraryClient client)
        {
            foreach (Book book1 in client.RentBook)
            {
                if (book1 == book) 
                    return true;
            }
            return false;
        }
        Library(List<Book> books)
        {
            _books = books;
        }
    }
}
