﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test2.Books;

namespace Test2.Library
{
    public class LibraryClient
    {
        public string FullName { get; set; }
        public uint Age { get; set; }
        public uint LibraryClientNumber { get; set; }
        public List<Book> RentBook { get; set; }
    }
}
